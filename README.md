# Remove untracked files from git repository after .gitignore update

1. Create local copy of your repository
1. Update .gitignore
1. Commit all changes
1. Remove everything from your repository

    ```bash
    git rm -r --cached .
    ```

1. Re-add everything

    ```bash
    git add .
    ```

1. Review git logs, there is a high chance some necessary files will be removed (libs etc.)
1. Commit changes
1. (If necessary) Copy removed files to your repository directory from local copy (i.e. *.user files etc). You can check commit logs to find out necessary file names)
